import Vue from 'vue';
import VuePerfectList from '../src/main';
import App from './components/App.vue';

Vue.config.productionTip = false;

Vue.use(VuePerfectList);

new Vue({
  render: (h) => h(App),
}).$mount('#app');
