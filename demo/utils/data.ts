import faker from 'faker';

export function genMessage () {
  return {
    id: faker.random.uuid(),
    avatar: faker.internet.avatar(),
    message: faker.lorem.text(),
  };
}