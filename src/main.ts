import { PluginObject } from 'vue';
import List from './components/List';

export default <PluginObject<void>>{
  install: (vm) => {
    vm.component('VuePerfectList', List);
  },
};
export const VuePerfectList = List;
export { Viewport } from './services/viewport';
export { ScrollTracker, EventName as ScrollTrackerEventName } from './services/scroll-tracker';
export { VirtualScroller, EventName as VirtualScrollerEventName, Item as VirtualScrollerItem } from './services/virtual-scroller';
export { getScrollContainer } from './utils/dom';
