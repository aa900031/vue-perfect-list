import { throttle } from 'throttle-debounce';
import { EventEmitter } from 'eventemitter3';
import { Viewport } from './viewport';
import { nextFrameTickAsync } from '../utils/sleep';

class SizeMap<K> {
  private _acc: number = 0;
  private _count: number = 0;
  private _store: Map<K, SizeItem> = new Map();
  private _defaultSize: number;

  constructor(defaultSize: number) {
    this._defaultSize = defaultSize;
  }

  get(key: K): SizeItem {
    const target = this._store.get(key);
    if (target) {
      return target;
    }

    return {
      acc: -1,
      size: this._getRunningSize(),
    };
  }

  set(key: K, value: SizeItem) {
    if (this._store.has(key)) {
      this._acc += value.size;
      this._acc -= this._store.get(key)!.size;
    } else {
      this._acc += value.size;
      this._count ++;
    }

    this._store.set(key, value);
  }

  private _getRunningSize(): number {
    const avgSize = Math.ceil(this._acc / this._count);

    return avgSize || this._defaultSize;
  }
}

export enum EventName {
  Updated = 'updated'
}

export type Events = {
  [EventName.Updated]: () => void
}

export class VirtualScroller<D> extends EventEmitter<Events, ThisType<VirtualScroller<D>>> {
  private _option: Option;

  private _data: D[];
  private _sizes: SizeMap<D>;
  private _lastScrollTop: number;
  private _visibleData: Item<D>[];
  private _afterBlankSize: number;
  private _beforeBlankSize: number;

  private _viewport: Viewport | null = null;
  private _handleScrollerScrollWithOpt: (() => void) | null = null;

  constructor(data: D[], option: Option) {
    super()
    this._option = Object.assign({}, {
      remainCount: 10,
    }, option);

    this._data = data;
    this._sizes = new SizeMap<D>(this._option.defaultItemSize);
    this._lastScrollTop = -1;
    this._afterBlankSize = 0
    this._beforeBlankSize = 0
    this._visibleData = []
  }

  get afterBlankSize(): number {
    return this._afterBlankSize;
  }

  get beforeBlankSize(): number {
    return this._beforeBlankSize;
  }

  get visibleData(): Item<D>[] {
    return this._visibleData;
  }

  mount(viewport: Viewport) {
    if (!viewport || !viewport.$el || !viewport.$scroller) {
      throw new Error();
    }
    this._viewport = viewport;

    this._handleScrollerScrollWithOpt = this._option.throttleWait
      ? throttle(this._option.throttleWait, this._handleScrollerScroll.bind(this))
      : () => window.requestAnimationFrame(this._handleScrollerScroll.bind(this));
    this._viewport.$scroller.addEventListener('scroll', this._handleScrollerScrollWithOpt!);
    this._lastScrollTop = this._viewport.scrollTop
    this._updateLayout(this._viewport.scrollTop);
  }

  unmount() {
    if (this._viewport) {
      this._handleScrollerScrollWithOpt
        && this._viewport.$scroller.removeEventListener('scroll', this._handleScrollerScrollWithOpt);
    }
  }

  updateData(data: D[]) {
    this._data = data;
    this._updateLayout(this._viewport!.scrollTop);
  }

  updateItemSize(item: D, size: number) {
    this._sizes.set(item, {
      acc: -1, size,
    });
  }

  async scrollToTop() {
    const { _viewport } = this;
    if (!_viewport) {
      return;
    }

    _viewport.scrollTop = 0;

    await nextFrameTickAsync()

    if (_viewport.scrollTop !== 0) {
      await this.scrollToTop();
    }
  }

  async scrollToBottom() {
    const { _viewport } = this;
    if (!_viewport) {
      return;
    }
    const currentScrollHeight = _viewport.scrollHeight - _viewport.clientHeight;
    _viewport.scrollTop = currentScrollHeight;

    await nextFrameTickAsync()

    const nextScrollHeight = _viewport.scrollHeight - _viewport.clientHeight;
    if (nextScrollHeight !== currentScrollHeight) {
      await this.scrollToBottom();
    }
  }

  private _getItemSize(item: D): number {
    return this._sizes.get(item).size;
  }

  private _handleScrollerScroll() {
    const { scrollTop } = this._viewport!;
    if (this._lastScrollTop === scrollTop) {
      return;
    }

    this._updateLayout(scrollTop);

    this._lastScrollTop = scrollTop;
  }

  private _updateLayout(scrollTop: number) {
    if (!Array.isArray(this._data)) {
      return;
    }

    if (this._data.length === 0) {
      this._beforeBlankSize = 0
      this._visibleData = []
      this._afterBlankSize = 0
    } else {
      const { firstItemIndex, beforeBlankSize } = this._calcBeforeBlank(scrollTop);
      this._beforeBlankSize = beforeBlankSize;

      const { lastItemIndex, data } = this._calcVisibleData(scrollTop, firstItemIndex, beforeBlankSize);
      this._visibleData = data

      const afterBlankSize = this._calcAfterBlank(lastItemIndex)
      this._afterBlankSize = afterBlankSize;
    }

    this.emit(EventName.Updated)
  }

  private _calcBeforeBlank(scrollTop: number) {
    const isScrollAreDecrement = this._lastScrollTop - scrollTop > 0;

    let index = 0;
    let beforeBlankSize = 0;

    while (true) {
      const item = this._data[index];
      const itemSize = this._getItemSize(item);
      if (beforeBlankSize + itemSize > (scrollTop + this._viewport!.offsetTop)) break;
      index += 1;
      beforeBlankSize += itemSize;
    }

    if (isScrollAreDecrement && index > 0) {
      for (let i = 0; i < this._option.remainCount!; i++) {
        if (index - 1 < 0) break;
        index -= 1;

        const item = this._data[index];
        if (!item) break;

        const itemSize = this._getItemSize(item);
        beforeBlankSize -= itemSize;
      }
    }

    return { beforeBlankSize, firstItemIndex: index };
  }

  private _calcVisibleData(scrollTop: number, firstItemIndex: number, offsetTop: number) {
    const isScrollAreIncrement = this._lastScrollTop - scrollTop < 0;
    const data: Item<D>[] = [];

    let accSize = offsetTop;
    let index = firstItemIndex;

    for (
      ;
      accSize - (scrollTop + this._viewport!.offsetTop) < this._viewport!.clientHeight
        && index < this._data.length;
      index += 1
    ) {
      const item = this._data[index];
      if (!item) continue;

      data.push({
        index,
        value: item,
      });
      accSize += this._getItemSize(item)
    }

    if (isScrollAreIncrement || firstItemIndex === 0) {
      for (let i = 0; i < this._option.remainCount!; i += 1) {
        const item = this._data[index];
        if (!item) break;

        data.push({
          index,
          value: item,
        });
        index += 1;
      }
    }

    return { lastItemIndex: index, data };
  }

  private _calcAfterBlank(lastItemIndex: number): number {
    return this._data
      .slice(lastItemIndex)
      .map((x, i) => {
        const item = this._data[i + lastItemIndex];
        return this._getItemSize(item);
      })
      .reduce((acc, size) => acc + size, 0);
  }
}

export interface SizeItem {
  size: number;
  acc: number;
}

export interface Option {
  defaultItemSize: number;
  remainCount?: number;
  throttleWait?: number;
}

export interface State<D> {
  beforeBlankSize: number;
  afterBlankSize: number;
  data: Item<D>[];
}

export interface Item<D> {
  index: number;
  value: D;
}
