import { getScrollContainer } from '../utils/dom';

export class Viewport {
  $el: HTMLElement;
  $scroller: Window | HTMLElement;
  offsetTop: number;

  constructor(el: HTMLElement, scroller?: Window | HTMLElement, vertical?: boolean) {
    if (!el) {
      throw new Error('attr.el is nil');
    }
    this.$el = el;

    if (!scroller) {
      const _scroller = getScrollContainer(el, vertical || true);
      if (!_scroller) {
        throw new Error('attr.el not have scroller');
      }
      scroller = _scroller;
    }
    this.$scroller = scroller;

    if (this.$scroller === window) {
      this.offsetTop = document.documentElement.offsetTop - this.$el.offsetTop;
    } else {
      this.offsetTop = (<HTMLElement>this.$scroller).offsetTop - this.$el.offsetTop;
    }
  }

  get scrollTop(): number {
    let offset: number = 0;
    const { $scroller } = this;

    if ($scroller === window) {
      offset = window.pageYOffset
    } else {
      offset = (<HTMLElement>$scroller).scrollTop;
    }

    return Math.ceil(offset);
  }

  set scrollTop(val: number) {
    const { $scroller } = this;

    if ($scroller === window) {
      (<Window>$scroller).scrollTo(0, val);
    } else {
      (<HTMLElement>$scroller).scrollTop = val;
    }
  }

  get scrollLeft(): number {
    let offset: number = 0;
    const { $scroller } = this;

    if ($scroller === window) {
      offset = window.pageXOffset;
    } else {
      offset = (<HTMLElement>$scroller).scrollLeft;
    }

    return Math.ceil(offset);
  }

  set scrollLeft(val: number) {
    const { $scroller } = this;

    if ($scroller === window) {
      (<Window>$scroller).scrollTo(val, 0);
    } else {
      (<HTMLElement>$scroller).scrollLeft = val;
    }
  }

  /**
   * 取得可見區域寬度
   */
  get clientHeight(): number {
    let height: number = 0;
    const { $scroller } = this;

    if ($scroller === window) {
      height = $scroller.innerHeight;
    } else {
      height = (<HTMLElement>$scroller).clientHeight;
    }

    return Math.ceil(height);
  }

  get clientWidth(): number {
    let width: number = 0;
    const { $scroller } = this;

    if ($scroller === window) {
      width = $scroller.innerWidth;
    } else {
      width = (<HTMLElement>$scroller).clientWidth;
    }

    return Math.ceil(width);
  }


  get scrollHeight(): number {
    let height: number = 0;
    const { $scroller } = this;

    if ($scroller === window) {
      height = document.documentElement.scrollHeight;
    } else {
      height = (<HTMLElement>$scroller).scrollHeight;
    }

    return Math.ceil(height);
  }

  get scrollWidth(): number {
    let width: number = 0;
    const { $scroller } = this;

    if ($scroller === window) {
      width = document.documentElement.scrollWidth;
    } else {
      width = (<HTMLElement>$scroller).scrollWidth;
    }

    return Math.ceil(width);
  }
}
