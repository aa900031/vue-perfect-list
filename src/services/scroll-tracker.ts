import { debounce } from 'throttle-debounce';
import { Viewport } from './viewport';
import { EventEmitter } from 'eventemitter3';

export class ScrollTracker extends EventEmitter<Events> {
  private _option: Option;

  private _viewport: Viewport | null = null;
  private _handleScrollWithOpt: (() => void) | null = null;

  private _reach: ScrollReach;
  private _lastScrollTop: number;
  private _lastScrollLeft: number;

  constructor(option: Option = {}) {
    super();

    this._option = Object.assign({}, {
      throttleWait: 100,
    }, option);

    this._lastScrollTop = -1;
    this._lastScrollLeft = -1;
    this._reach = { x: null, y: null };
  }

  mount(viewport: Viewport) {
    if (!viewport || !viewport.$el || !viewport.$scroller) {
      throw new Error();
    }

    this._viewport = viewport;
    this._handleScrollWithOpt = () => window.requestAnimationFrame(this._handleScroll.bind(this));
    this._viewport.$scroller.addEventListener('scroll', this._handleScrollWithOpt);
    this._lastScrollTop = this._viewport.scrollTop;
    this._lastScrollLeft = this._viewport.scrollLeft;
  }

  unmount() {
    if (this._viewport) {
      this._handleScrollWithOpt
        && this._viewport.$scroller.removeEventListener('scroll', this._handleScrollWithOpt);
    }
  }

  private _handleScroll() {
    const { scrollTop, scrollHeight, clientHeight } = this._viewport!;
    const { scrollLeft, scrollWidth, clientWidth } = this._viewport!;
    const diffScrollTop = scrollTop - this._lastScrollTop;
    const diffScrollLeft = scrollLeft - this._lastScrollLeft;

    if (this._updateScrollReach('y', scrollHeight, clientHeight, scrollTop, diffScrollTop)) {
      switch (this._reach.y) {
        case 'top': this.emit(EventName.ReachTop); break;
        case 'bottom': this.emit(EventName.ReachBottom); break;
      }
    }

    if (this._updateScrollReach('x', scrollWidth, clientWidth, scrollLeft, diffScrollLeft)) {
      switch (this._reach.x) {
        case 'start': this.emit(EventName.ReachStart); break;
        case 'end': this.emit(EventName.ReachEnd); break;
      }
    }

    this._lastScrollTop = scrollTop;
    this._lastScrollLeft = scrollLeft;
  }

  private _updateScrollReach(
    axis: 'x' | 'y',
    /** 可滾動內容的尺寸 */
    scrollSize: number,
    /** 可視範圍尺寸 */
    clientSize: number,
    /** 已滾動範圍 */
    scrollOffset: number,
    /** 距離上一次滾動 */
    diffScrollOffset: number,
  ): boolean {
    this._reach[axis] = null;

    if (scrollOffset < 1) {
      switch (axis) {
        case 'x': this._reach[axis] = 'start'; break;
        case 'y': this._reach[axis] = 'top'; break;
      }
    }

    if (scrollOffset > scrollSize - clientSize - 1) {
      switch (axis) {
        case 'x': this._reach[axis] = 'end'; break;
        case 'y': this._reach[axis] = 'bottom'; break;
      }
    }

    if (!this._reach[axis] || !diffScrollOffset) {
      return false;
    }

    return true;
  }
}

export type Events = {
  [EventName.ReachTop]: () => void
  [EventName.ReachBottom]: () => void
  [EventName.ReachStart]: () => void
  [EventName.ReachEnd]: () => void
}

export enum EventName {
  ReachTop = 'reachTop',
  ReachBottom = 'reachBottom',
  ReachStart = 'reachStart',
  ReachEnd = 'reachEnd',
}

export interface Option {
  debounceEventWait?: number;
}

export interface ScrollReach {
  x: 'start' | 'end' | null;
  y: 'top' | 'bottom' | null;
}
