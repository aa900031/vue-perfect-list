import Vue from 'vue';
import { ThisTypedComponentOptionsWithRecordProps } from 'vue/types/options';
import { CombinedVueInstance } from 'vue/types/vue';
import { VirtualScroller, Option as VirtualScrollerOption, Item as VirtualScrollerItem, EventName as VirtualScrollerEventName } from '../services/virtual-scroller';
import { ScrollTracker, Option as ScrollTrackerOption, EventName as ScrollTrackerEventName } from '../services/scroll-tracker';
import { getScrollContainer } from '../utils/dom';
import { Viewport } from '../services/viewport';

export type ComponentOption = ThisTypedComponentOptionsWithRecordProps<Instance, Data, Methods, Computed, Props>;
export type ComponentInstance = CombinedVueInstance<Instance, Data, Methods, Computed, Props>;
export interface Instance extends Vue {
  _virtualScroller: VirtualScroller<Item>;
  _scrollTracker: ScrollTracker;
  _viewport: Viewport;
  $el: HTMLElement;
  $listeners: {
    reachTop: Function;
    reachBottom: Function;
  };
}
export interface Data {
  vscroll: null | {
    afterBlankSize: number
    beforeBlankSize: number
    visibleData: VirtualScrollerItem<Item>[]
  }
}
export type Methods = {
  genScopedSlotProp(item: Item | VirtualScrollerItem<Item>, index: number): SlotDefaultProps;
  handleScrollReachTop(): void;
  handleScrollReachBottom(): void;
  handleVirtualScrollerUpdated(): void;
  scrollToTop(): Promise<boolean>;
  scrollToBottom(): Promise<boolean>;
}
export interface Computed {
  isEnableVirtualScroll: boolean;
  isEnableScrollTracker: boolean;
  isNeedRenderList: boolean;
  wrapperStyle: Partial<CSSStyleDeclaration>;
  currentData: Item[] | VirtualScrollerItem<Item>[];
}
export interface Props {
  data: Item[];
  virtualScrollerOption: VirtualScrollerOption;
  scrollTrackerOption: ScrollTrackerOption;
}
export interface SlotDefaultProps {
  index: number;
  data: Item;
  updateSize(size: number): void;
}
type Item = string | number | object;

const options: ComponentOption = {
  props: {
    data: {
      type: Array,
      required: true
    },
    virtualScrollerOption: {
      type: Object
    },
    scrollTrackerOption: {
      type: Object
    },
  },

  data() {
    if (!!this.virtualScrollerOption) {
      this._virtualScroller = new VirtualScroller(this.data, this.virtualScrollerOption);
    }

    if (!!this.scrollTrackerOption) {
      this._scrollTracker = new ScrollTracker(this.scrollTrackerOption);
    }

    return {
      vscroll: this._virtualScroller ? {
        afterBlankSize: this._virtualScroller.afterBlankSize,
        beforeBlankSize: this._virtualScroller.beforeBlankSize,
        visibleData: this._virtualScroller.visibleData,
      } : null
    }
  },

  mounted() {
    const { $el } = this
    if (!$el) return

    this._viewport = new Viewport($el, getScrollContainer(<HTMLElement>$el, true)!);

    if (this.isEnableVirtualScroll) {
      this._virtualScroller.on(VirtualScrollerEventName.Updated, this.handleVirtualScrollerUpdated)
      this._virtualScroller.mount(this._viewport);
    }

    if (this.isEnableScrollTracker) {
      this.$listeners.reachTop
        && this._scrollTracker.on(ScrollTrackerEventName.ReachTop, this.handleScrollReachTop);
      this.$listeners.reachBottom
        && this._scrollTracker.on(ScrollTrackerEventName.ReachBottom, this.handleScrollReachBottom);
      this._scrollTracker.mount(this._viewport);
    }
  },

  beforeDestroy() {
    this._virtualScroller
      && this._virtualScroller.unmount();
    this._scrollTracker
      && this._scrollTracker.unmount();
  },

  render(h) {
    return h('div', {
      style: this.wrapperStyle,
    }, [
      this.isNeedRenderList
        ? (<any>this)._l(this.currentData, (item: Item | VirtualScrollerItem<Item>, index: number) => {
          return this.$scopedSlots.default!(this.genScopedSlotProp(item, index));
        })
        : (<any>this)._e() // Vue private API
    ]);
  },

  computed: {
    isEnableVirtualScroll(): boolean {
      return !!this.virtualScrollerOption;
    },

    isEnableScrollTracker(): boolean {
      return !!this.scrollTrackerOption;
    },

    isNeedRenderList(): boolean {
      return !!this.$scopedSlots.default && !!this.currentData;
    },

    wrapperStyle() {
      const style: Partial<CSSStyleDeclaration> = {};

      if (this.isEnableVirtualScroll && this.vscroll) {
        style.paddingTop = `${this.vscroll.beforeBlankSize}px`;
        style.paddingBottom = `${this.vscroll.afterBlankSize}px`;
      }

      return style;
    },

    currentData() {
      if (this.isEnableVirtualScroll && this.vscroll) {
        return this.vscroll.visibleData;
      }
      return this.data;
    },
  },

  methods: {
    scrollToTop() {
      return new Promise((resolve, reject) => {
        if (this._virtualScroller) {
          this._virtualScroller.scrollToTop().then(() => {
            resolve(true);
          });
          return;
        }

        if (this._viewport) {
          this._viewport.scrollTop = 0;
          resolve(true);
          return;
        }

        resolve(false);
      });
    },

    scrollToBottom() {
      return new Promise((resolve, reject) => {
        if (this._virtualScroller) {
          this._virtualScroller.scrollToBottom().then(() => {
            resolve(true);
          });
          return;
        }

        if (this._viewport) {
          this._viewport.scrollTop = this._viewport.scrollHeight;
          resolve(true)
          return;
        }
        resolve(false);
      });
    },

    genScopedSlotProp(item, index) {
      if (this.isEnableVirtualScroll && this.vscroll) {
        const _item: VirtualScrollerItem<Item> = item as VirtualScrollerItem<Item>

        return {
          index: _item.index,
          data: _item.value,
          updateSize: (size: number) => {
            this._virtualScroller.updateItemSize(_item.value, size);
          },
        }
      }

      return {
        index,
        data: item,
        updateSize: () => {},
      };
    },

    handleScrollReachTop() {
      this.$emit('reachTop');
    },

    handleScrollReachBottom() {
      this.$emit('reachBottom');
    },

    handleVirtualScrollerUpdated() {
      const { vscroll, _virtualScroller } = this
      if (!_virtualScroller) return
      if (!vscroll) return

      vscroll.afterBlankSize = _virtualScroller.afterBlankSize
      vscroll.beforeBlankSize = _virtualScroller.beforeBlankSize
      vscroll.visibleData = _virtualScroller.visibleData
    }
  },

  watch: {
    data(val: Item[]) {
      if (this.isEnableVirtualScroll) {
        this._virtualScroller.updateData(val);
      }
    },
  }
}
export default options
