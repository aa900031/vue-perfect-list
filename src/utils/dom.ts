import Vue from 'vue';
import { camelCase } from './string';

const isServer = typeof window === 'undefined';

export const getScrollContainer = (el: HTMLElement, vertical: boolean): HTMLElement | Window | null => {
  if (isServer) return null;

  let parent: HTMLElement | null = el;
  while (parent) {
    if ([window, document, document.documentElement].includes(parent)) {
      return window;
    }
    if (isScroll(parent, vertical)) {
      return parent;
    }
    parent = parent.parentElement;
  }

  return parent;
};

export const isScroll = (el: HTMLElement, vertical: boolean): boolean => {
  if (isServer) return false;

  const determinedDirection = vertical !== null || vertical !== undefined;
  const overflow = determinedDirection
    ? vertical
      ? getStyle(el, 'overflow-y')
      : getStyle(el, 'overflow-x')
    : getStyle(el, 'overflow');

  if (!overflow) {
    return false;
  }

  return !!overflow.match(/(scroll|auto)/);
};

export const getStyle = (element: HTMLElement, styleName: string): string | null | void => {
  if (isServer) return;
  if (!element || !styleName) return null;

  styleName = camelCase(styleName);
  if (styleName === 'float') {
    styleName = 'cssFloat';
  }

  return element.style[styleName as any] || (() => {
    try {
      const computed = document.defaultView!.getComputedStyle(element, '');
      return computed ? computed[styleName as any] : null;
    } catch (error) {
      return element.style[styleName as any];
    }
  })();
};
