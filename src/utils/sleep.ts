export const nextFrameTick = (cb: Function) => {
  requestAnimationFrame(() => setTimeout(cb, 1));
};

export const nextFrameTickAsync = () => new Promise((resolve) => {
  nextFrameTick(resolve);
});
