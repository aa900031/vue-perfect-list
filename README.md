# vue-perfect-list
![npm download of week](https://img.shields.io/npm/dw/vue-perfect-list)
![npm version](https://img.shields.io/npm/v/vue-perfect-list)
![npm bundle size](https://img.shields.io/bundlephobia/minzip/vue-perfect-list)

## Installation
```bash
npm install vue-perfect-list
// or
yarn add vue-perfect-list
```

## Import with CommonJS
```javascript
import Vue from 'vue'
import VuePerfectList from 'vue-perfect-list'

Vue.use(VuePerfectList)
```

## Import on Browser
```html
<script src="vue.js"></script>
<script src="vue-perfect-list/dist/vue-perfect-list.umd.min.js"></script>
```

## Usage
```html
<template>
  <VuePerfectList
    :data="list"
    :virtualScrollerOption="{
      defaultItemSize: 30,
      remainCount: 15,
    }"
    :scrollTrackerOption="{
      debounceEventWait: 100,
    }"
    @reachTop="handleScrollReachTop"
    @reachBottom="handleScrollReachBottom">
    <template v-slot="{ data, index, updateSize }">
      <Item
        :key="index"
        :data="data"
        @update-size="updateSize">
      </Item>
    </template>
  </VuePerfectList>
</template>

<script>
export default {
  data() {
    return {
      list: [],
    };
  },

  methods: {
    handleScrollReachTop() {
      console.log('reach on top');
    },

    handleScrollReachBottom() {
      console.log('reach on bottom');
    },
  },
}
</script>
```

## Component: VuePerfectList
### Props
- `data` : `any[]`
- `virtualScrollerOption` : `?object`
  - `defaultItemSize`: `number`
  - `remainCount` : `?number`
  - `throttleWait` : `?number`
- `scrollTrackerOption` : `?object`
  - `throttleWait` : `?number`


### Events
- `reachTop` : `(): void`
- `reachBottom` : `(): void`;

### ScopSlotProps
- `index` : `number`
- `data` : `any`
- `updateSize` : `(dom?: HTMLElement): void`

### Methods
- `scrollToTop` : `(): Promise<void>`
- `scrollToBottom` : `(): Promise<void>`


## Class: Viewport
`new Viewport(el, scroll, vertical)`
- `el` : `DOMElement`
- `scroll` : `?DOMElement`
- `vertical` : `?boolean`

### Members
- `scrollTop` : `number`
- `scrollLeft` : `number`
- `clientHeight` : `number`
- `clientWidth` : `number`
- `scrollHeight` : `number`
- `scrollWidth` : `number`

## Class: ScrollTracker
`new ScrollTracker(option)`
- `option` : `object`
  - `debounceEventWait` : `?number`

### Methods
- `mount` : `(viewport: Viewport): void`
- `unmount` : `(): void`
- `addEventListener` : `(event: string, handler): void`
- `removeEventListener` : `(event: string, handler: ): void`

### Events
`reachTop`, `reachBottom`, `reachRight`, `reachLeft`

## Class: VirtualScroller
`new VirtualScroller(data, option)`
- `data` : `any[]`
- `option` : `object`
  - `defaultItemSize` : `number`
  - `remainCount` : `?number`
  - `throttleWait` : `?number`

### Members
- `state` : `object`
  - `beforeBlankSize` : `nubmer`
  - `afterBlankSize` : `nubmer`
  - `data` : `Item[]`
    - `index` : `number`
    - `value` : `any`

### Methods
- `mount` : `(viewport: Viewport): void`
- `unmount` : `(): void`
- `updateData` : `(data: any[]): void`
- `updateItemSize` : `(item: any, size: number): void`
- `scrollToTop` : `(): Promise<void>`
- `scrollToBottom` : `(): Promise<void>`

## Run demo project
```bash
yarn dev
```