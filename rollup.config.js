import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from 'rollup-plugin-typescript2';
import dts from 'rollup-plugin-dts';
import { terser } from 'rollup-plugin-terser';
import pkg from './package.json'

const mainModuleConfig = {
  input: 'src/main.ts',

  output: [{
    file: pkg.main,
    format: 'cjs',
  },{
    file: pkg.module,
    format: 'es',
  }],

  plugins: [
    typescript(),
  ],

  external: [
    'vue',
    'eventemitter3',
    'throttle-debounce',
  ],
}

const mainUnpkgConfig = {
  input: 'src/main.ts',

  output: {
    file: pkg.unpkg,
    format: 'umd',
    name: 'VuePerfectList',
    globals: {
      vue: 'Vue',
    },
  },

  plugins: [
    resolve(),
    commonjs(),
    typescript(),
    terser(),
  ],

  external: [
    'vue',
  ],
}

const dtsConfig = {
  input: 'dist-types/main.d.ts',

  output: {
    file: pkg.types,
    format: 'es',
  },

  plugins: [
    dts({
      compilerOptions: {
        declaration: true,
        declarationDir: "./dist-types",
      }
    }),
  ],
}

export default [
  mainModuleConfig,
  mainUnpkgConfig,
  dtsConfig,
];
